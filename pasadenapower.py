import selenium
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.action_chains import ActionChains
import time
from ConfigParser import ConfigParser
import smtplib

class PasadenaPower(object):
    
    PRIVATE_CONFIG_FILE = '/Users/davideynon/Desktop/pasadenapower.properties'
    PROPERTIES_FILE = '/Users/davideynon/Projects/pasadenapower/pasadenapower.properties'
    
    accounts = ['438_water_sewer', '438_electric_trash', '440_all']
    
    #login_url = 'https://eservices.cityofpasadena.net/IwrWP/Logon.aspx'
    login_url = 'https://ww5.cityofpasadena.net/water-and-power/'
    
    
    def _get_property(self, config, section='default', private=False):
        """Gets properties. Also allows for private config file. This is not added to repository."""
        c = ConfigParser()
        if private:
            c.read(self.PRIVATE_CONFIG_FILE)
        else:
            c.read(self.PROPERTIES_FILE)
        return c.get(section, config)
    
    def _get_private_properties(self, config, section='default'):
        """convenience/passthrough to _get_property()"""
        return self._get_property(config, section, private=True)
    
    def _get_account_number(self, account):
        """convenience method"""
        return self._get_private_properties('account_number', account)
    
    def _get_account_password(self, account):
        """convenience method"""
        return self._get_private_properties('account_password', account)
    
    def get_driver(self, url=None):
        """Opens and returns WebDriver instance. Global variable"""
        self.driver = WebDriver(executable_path="/Users/davideynon/Projects/geckodriver")
    
    def quit(self, driver=None):
        """Closes WebDriver instance"""
        self.driver.quit()
        return
    
    def get_login_page(self):
        """Navigate to login page"""
        self.driver.get(self.login_url)
        return
    
    def login(self, account):
        """Perform login"""
        account_number = self._get_account_number(account)
        account_password = self._get_account_password(account)

        get_account_number_element = self.driver.find_element_by_css_selector(self._get_property('account_number'))
        get_account_password_element = self.driver.find_element_by_css_selector(self._get_property('account_password'))
        get_form = self.driver.find_element_by_css_selector(self._get_property('submit_form'))

        """Set account number"""
        time.sleep(10);
        get_account_number_element.send_keys(account_number)
        get_account_password_element.send_keys(account_password)
        get_form.submit();

        return
    
    def logout(self):
        """Perform logout"""
        self.driver.find_element_by_css_selector(self._get_property('logout')).click()
        return
    
    def disable_prompt(self):
        """a prompt asking user to update info appears. It will appear every time (local cookie). This handles it"""
        for x in range(0,10):
            if self.driver.find_elements_by_id(self._get_property('prompt_update_info')):
                self.driver.find_element_by_id(self._get_property('prompt_update_info_action_later')).click()
                return
            else:
                time.sleep(3)
    
    def get_info(self):
        """Gets info. Returns in form of data dictionary"""
        info = {}
        items = ['number', 'address_1','address_2','amount_due','due_date']
        for i in items:
            info[i] = self.driver.find_element_by_id(self._get_property(i)).text
        return info
    
    @property
    def email(self):
        """Gets email login, address, etc. info from config file"""
        email = {}
        email['from'] = self._get_private_properties('from', 'email')
        email['to'] = self._get_private_properties('to', 'email')
        email['password'] = self._get_private_properties('password', 'email')        
        return email
    
    def screenshot(self, driver):
        """Get screenshot of page"""
        return
    
    def send_email(self, msg=''):
        subject = 'Requested Automated Alert from Pasadena Power Account Status'
        msg = 'Subject:%s\n\n%s' % (subject, msg)
            
        # The actual mail send
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(self.email['from'], self.email['password'])
        emails = self.email['to'].split(',')
        for email in emails:
            server.sendmail(self.email['from'], email, msg)
        server.quit()
    
    def _form_email(self, info):
        content = ''
        for i in info:
            content += 'Account Number: \t%s \n' % i['number']
            content += 'Address:        \t%s %s \n' % (i['address_1'], i['address_2'])
            content += 'Amount due:     \t%s \n' % i['amount_due']
            content += 'Due Date:       \t%s \n' % i['due_date']
            content += '\n\n\n'
        content += self.login_url
        return content
    
    def main(self):
        self.get_driver()
        self.get_login_page()
        
        info = []
        for account in self.accounts:
            self.login(account)
            self.disable_prompt()
            info.append(self.get_info())
            self.logout()
            time.sleep(10)
        
        self.send_email(self._form_email(info))
        self.quit()
    
if __name__=="__main__":
    PasadenaPower().main()
